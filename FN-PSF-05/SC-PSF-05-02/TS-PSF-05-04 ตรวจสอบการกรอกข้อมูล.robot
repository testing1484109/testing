**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL_manageFollowStu}     http://127.0.0.1:8080/followStudent
${web_browser}             chrome
${txt_stuConReg}           CS    
${txt_stuConReg2}            
${txt_stuConReg3}          555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555  
${btnSunmit}               ok
${stuConReg}               stuConReg
${btnFollow}               xpath=//a[@href="/followStudentCon/4"]
***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${stuConReg}        ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}
***Test Case ***
TC-PSF-05-04-01 ตรวจสอบการกรอกข้อมูลแผนการเรียน 
    เปิดหน้าจอ                ${URL_manageFollowStu}              ${web_browser}
    กดลิงค์รูป                 ${btnFollow}
    กรอกข้อมูล                ${txt_stuConReg}
    กดปุ่ม                    ${btnSunmit} 
TC-PSF-05-04-02 ตรวจสอบการกรอกข้อมูลแผนการเรียน โดยไม่กรอกอะไรเลย
    เปิดหน้าจอ                ${URL_manageFollowStu}              ${web_browser}
    กดลิงค์รูป                 ${btnFollow}
    กรอกข้อมูล                ${txt_stuConReg2}
    กดปุ่ม                    ${btnSunmit} 
TC-PSF-05-04-03 ตรวจสอบการกรอกข้อมูลแผนการเรียน รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอ                ${URL_manageFollowStu}              ${web_browser}     
    กดลิงค์รูป                 ${btnFollow}           
    กรอกข้อมูล                ${txt_stuConReg3}
    กดปุ่ม                    ${btnSunmit} 

