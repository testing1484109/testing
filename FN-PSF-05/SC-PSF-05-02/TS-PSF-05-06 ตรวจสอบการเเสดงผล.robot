**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL_manageFollowStu}     http://127.0.0.1:8080/followStudent
${web_browser}             chrome
${btnSunmit}               ok
${btnFollow}               xpath=//a[@href="/followStudentCon/2"]
${txt_stuConReg}           CS   
${stuConReg}               stuConReg
***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${stuConReg}        ${txt}

    
กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุมเข้าสู่ระบบ
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-05-06-01 ตรวจสอบการแสดงผลข้อมูลติตามนิสิตรอการพินิจ
    เปิดหน้าจอ                ${URL_manageFollowStu}              ${web_browser}
    กดลิงค์รูป                 ${btnFollow}
    กรอกข้อมูล                ${txt_stuConReg}
    กดปุ่ม                    ${btnSunmit} 

