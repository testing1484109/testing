**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL_manageStuEnp}     http://127.0.0.1:8080/manageStudentEnp
${web_browser}          chrome
${btnSearch}            btnsearch
${btnAdd}               btnAdd
${btnEdit}              xpath=//a[@href="/manageEnp_edit/9"]
${btnhDel}              xpath=//a[@href="/delEnp/9"]

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กรอกข้อมูล
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-04-02-01 ตรวจสอบการกดปุ่มค้นหา
    เปิดหน้าจอ                ${URL_manageStuEnp}              ${web_browser}                
    กดปุ่ม                    ${btnSearch}    

TC-PSF-04-02-02 ตรวจสอบการกดปุ่มเพิ่มข้อมูล
    เปิดหน้าจอ                ${URL_manageStuEnp}              ${web_browser}                
    กดปุ่ม                    ${btnAdd}  

TC-PSF-04-02-03 ตรวจสอบการกดปุ่มแก้ไข
    เปิดหน้าจอ                ${URL_manageStuEnp}              ${web_browser}
    กดลิงค์รูป                 ${btnEdit}

TC-PSF-04-02-04 ตรวจสอบการกดปุ่มลบ
    เปิดหน้าจอ                ${URL_manageStuEnp}              ${web_browser}
    กดลิงค์รูป                 ${btnEdit}
