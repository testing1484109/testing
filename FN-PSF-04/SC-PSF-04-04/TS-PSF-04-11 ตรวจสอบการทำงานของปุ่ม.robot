**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuEnp}        http://127.0.0.1:8080/manageStudentEnp
${web_browser}             chrome

${btnSave}                 ok
${btnCancle}               cancel
${btmEdit}                  xpath=//a[@href="/manageEnp_edit/9"]
${btmDel}                  xpath=//a[@href="/delEnp/9"]

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-04-11-01 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ                ${URL_manageStuEnp}           ${web_browser}
    กดลิงค์รูป                 ${btmEdit}   
    กดปุ่ม                    ${btnCancle} 

TC-PSF-04-11-02 ตรวจสอบการกดปุ่มตกลง
    เปิดหน้าจอ                ${URL_manageStuEnp}           ${web_browser}
    กดลิงค์รูป                 ${btmEdit}   
    กดปุ่ม                    ${btnSave} 



























