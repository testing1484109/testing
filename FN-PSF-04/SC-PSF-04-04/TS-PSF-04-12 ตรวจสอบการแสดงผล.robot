**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuEnp}        http://127.0.0.1:8080/manageStudentEnp
${web_browser}             chrome

${btnSave}                 ok
${btnCancle}               cancel
${btmDel}                  xpath=//a[@href="/delEnp/20"]

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-03-13-01 ตรวจสอบการแสดงผลลบข้อมูล
    เปิดหน้าจอ                ${URL_manageStuEnp}           ${web_browser}
    กดลิงค์รูป                 ${btmDel}   
    กดปุ่ม                    ${btnSave} 




























