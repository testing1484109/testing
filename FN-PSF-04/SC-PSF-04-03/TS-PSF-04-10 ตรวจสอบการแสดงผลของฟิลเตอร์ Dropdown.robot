**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuEnp}        http://127.0.0.1:8080/manageStudentEnp
${web_browser}             chrome

${txt_stuLevelEnp}         ตรี พิเศษ
${txt_stuStatusEnp}        10

${btnSave}                 ok
${btnCancle}               cancel
${btnEdit}                 xpath=//a[@href="/manageEnp_edit/9"]

${stuID}                   studentIDEnp
${stuName}                 nameStuEnp
${stuLevelEnp}             stuLevelEnp
${stuGpaEnp}               stuGpaEnp
${stuStatusEnp}            stuStatusEnp
${creditPassedEnp}         creditPassedEnp
${creditDownEnp}           creditDownEnp

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}          ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${var}          ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กด Dropdown
    [Arguments]                 ${dropdown}     ${txt}
    [Documentation]             ใช้กดDropdown
    Select From List By Label   ${dropdown}    ${txt}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

*** Test Case ***
TC-PSF-04-10-01 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลระดับ
    เปิดหน้าจอ                ${URL_manageStuEnp}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}   
    กด Dropdown             ${stuLevelEnp}                ${txt_stuLevelEnp}
    กดปุ่ม                    ${btnSave} 

TC-PSF-04-10-02 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลสถานภาพ
    เปิดหน้าจอ                ${URL_manageStuEnp}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}   
    กด Dropdown             ${stuStatusEnp}               ${txt_stuStatusEnp}
    กดปุ่ม                    ${btnSave} 


