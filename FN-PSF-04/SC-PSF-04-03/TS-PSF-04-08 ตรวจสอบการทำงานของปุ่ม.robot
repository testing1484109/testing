**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuEnp}        http://127.0.0.1:8080/manageStudentEnp
${web_browser}             chrome

${txt_stuID}               586660044
${txt_name}                ธันวา เเสนสุข
${txt_gpa}                 2.00
${txt_crditPass}           3.00
${txt_crditDownEnp}        1.50
${txt_stuLevelEnp}         ตรี พิเศษ
${txt_stuStatusEnp}        12

${btnSave}                 ok
${btnCancle}               cancel
${btnEdit}                 xpath=//a[@href="/manageEnp_edit/9"]

${stuID}                   studentIDEnp
${stuName}                 nameStuEnp
${stuLevelEnp}             stuLevelEnp
${stuGpaEnp}               stuGpaEnp
${stuStatusEnp}            stuStatusEnp
${creditPassedEnp}         creditPassedEnp
${creditDownEnp}           creditDownEnp

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}          ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${var}          ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กด Dropdown
    [Arguments]                 ${dropdown}     ${txt}
    [Documentation]             ใช้กดDropdown
    Select From List By Label   ${dropdown}     ${txt}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-04-05-01 ตรวจสอบการกดปุ่มบันทึก
    เปิดหน้าจอ                ${URL_manageStuEnp}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}   
    กรอกข้อมูล                ${stuID}                      ${txt_stuID}
    กรอกข้อมูล                ${stuName}                    ${txt_name}
    กด Dropdown             ${stuLevelEnp}                ${txt_stuLevelEnp}
    กรอกข้อมูล                ${stuGpaEnp}                  ${txt_gpa}
    กด Dropdown             ${stuStatusEnp}               ${txt_stuStatusEnp}
    กรอกข้อมูล                ${creditPassedEnp}            ${txt_crditPass}
    กรอกข้อมูล                ${creditDownEnp}              ${txt_crditDownEnp}
    กดปุ่ม                    ${btnSave} 

TC-PSF-04-05-02 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ                ${URL_manageStuEnp}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}   
    กรอกข้อมูล                ${stuID}                      ${txt_stuID}
    กรอกข้อมูล                ${stuName}                    ${txt_name}
    กด Dropdown             ${stuLevelEnp}                ${txt_stuLevelEnp}
    กรอกข้อมูล                ${stuGpaEnp}                  ${txt_gpa}
    กด Dropdown             ${stuStatusEnp}               ${txt_stuStatusEnp}
    กรอกข้อมูล                ${creditPassedEnp}            ${txt_crditPass}
    กรอกข้อมูล                ${creditDownEnp}              ${txt_crditDownEnp}
    กดปุ่ม                    ${btnCancle} 





























