**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuEnpAdd}     http://127.0.0.1:8080/manageStudentEnp_add
${web_browser}             chrome

${txt_stuLevelEnp}         ตรี พิเศษ
${txt_stuStatusEnp}        10
${txt_considertime}        2
${txt_considerEnp}         โปรต่ำ

${stuLevelEnp}             stuLevelEnp
${stuStatusEnp}            stuStatusEnp
${considertime}            considertime
${considerEnp}             considerEnp

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กด Dropdown
    [Arguments]                 ${dropdown}     ${txt}
    [Documentation]             ใช้กดDropdown
    Select From List By Label   ${dropdown}    ${txt}

***Test Case ***
TC-PSF-03-06-01 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลระดับ
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}        ${web_browser}
    กด Dropdown             ${stuLevelEnp}                ${txt_stuLevelEnp}

TC-PSF-03-06-02 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลสถานภาพ
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}        ${web_browser}
    กด Dropdown             ${stuStatusEnp}               ${txt_stuStatusEnp}





























