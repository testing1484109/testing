**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuAdd}        http://127.0.0.1:8080/manageStudentEnp_add
${web_browser}             chrome

${txt_stuID}               586660044
${txt_name}                ธันวา เเสนสุข
${txt_gpa}                 2.00
${txt_crditPass}           3.00
${txt_crditDownEnp}        1.50
${txt_stuLevelEnp}         ตรี พิเศษ
${txt_stuStatusEnp}        10

${btnSave}                 ok
${btnCancle}               cancel
${stuID}                   studentIDEnp
${stuName}                 nameStuEnp
${stuLevelEnp}             stuLevelEnp
${stuGpaEnp}               stuGpaEnp
${stuStatusEnp}            stuStatusEnp
${creditPassedEnp}         creditPassedEnp
${creditDownEnp}           creditDownEnp
${considertime}            considertime
${considerEnp}             considerEnp

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}          ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${var}          ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กด Dropdown
    [Arguments]                 ${dropdown}     ${txt}
    [Documentation]             ใช้กดDropdown
    Select From List By Label   ${dropdown}    ${txt}

***Test Case ***
TC-PSF-03-05-01 ตรวจสอบการกดปุ่มบันทึก
    เปิดหน้าจอ                ${URL_manageStuAdd}           ${web_browser}
    กรอกข้อมูล                ${stuID}                      ${txt_stuID}
    กรอกข้อมูล                ${stuName}                    ${txt_name}
    กด Dropdown             ${stuLevelEnp}                ${txt_stuLevelEnp}
    กรอกข้อมูล                ${stuGpaEnp}                  ${txt_gpa}
    กด Dropdown             ${stuStatusEnp}               ${txt_stuStatusEnp}
    กรอกข้อมูล                ${creditPassedEnp}            ${txt_crditPass}
    กรอกข้อมูล                ${creditDownEnp}              ${txt_crditDownEnp}
    กดปุ่ม                    ${btnSave} 

TC-PSF-03-05-02 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ                ${URL_manageStuAdd}           ${web_browser}
    กรอกข้อมูล                ${stuID}                      ${txt_stuID}
    กรอกข้อมูล                ${stuName}                    ${txt_name}
    กด Dropdown             ${stuLevelEnp}                ${txt_stuLevelEnp}
    กรอกข้อมูล                ${stuGpaEnp}                  ${txt_gpa}
    กด Dropdown             ${stuStatusEnp}               ${txt_stuStatusEnp}
    กรอกข้อมูล                ${creditPassedEnp}            ${txt_crditPass}
    กรอกข้อมูล                ${creditDownEnp}              ${txt_crditDownEnp}           
    กดปุ่ม                    ${btnCancle} 





























