**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuEnpAdd}     http://127.0.0.1:8080/manageStudentEnp_add
${web_browser}             chrome

${txt_stuID}               586660044
${txt_stuID2}              63160200
${txt_stuID3}  
${txt_stuID4}              $%^&*12354@jfof;    

${txt_name}                ธันวา เเสนสุข
${txt_name2}               ธิดารัตน์ โคตรพัฒน์5555555555555555555555555555555555555555555555555
${txt_name3}               
${txt_name4}               $%^&*12354@jfof;

${txt_gpa}                 2.00
${txt_gpa2}                1.11111111
${txt_gpa3}                
${txt_gpa4}                $%^&*12354@jfof;

${txt_crditPass}           3.00
${txt_crditPass2}          3.00
${txt_crditPass3}          
${txt_crditPass4}          $%^&*12354@jfof;

${txt_crditDownEnp}        1.50
${txt_crditDownEnp2}       230.0
${txt_crditDownEnp3}       
${txt_crditDownEnp4}       $%^&*12354@jfof;

${btnAdd}                  btnAdd
${btnSave}                 ok
${btnCancle}               cancel
${stuID}                   studentIDEnp
${stuName}                 nameStuEnp
${stuLevelEnp}             stuLevelEnp
${stuGpaEnp}               stuGpaEnp
${stuStatusEnp}            stuStatusEnp
${creditPassedEnp}         creditPassedEnp
${creditDownEnp}           creditDownEnp
${considertime}            considertime

***Keyword***
เปิดหน้าจอ
    [Arguments]    ${link_web}    ${web_browser}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser    ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]    ${var}    ${txt}
    [Documentation]      กรอกข้อมูล
    Input Text    ${var}    ${txt}

กดปุ่ม
    [Arguments]    ${btn}
    [Documentation]      กดปุ่ม
    Click Button    ${btn}

***Test Case ***
TC-PSF-04-04-01 ตรวจสอบการกรอกข้อมูลรหัสนิสิต
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuID}                      ${txt_stuID} 

TC-PSF-43-04-02 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuName}                    ${txt_name}

TC-PSF-04-04-03 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuGpaEnp}                  ${txt_gpa}

TC-PSF-04-04-04 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${creditPassedEnp}            ${txt_crditPass}

TC-PSF-04-04-05 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${creditDownEnp}              ${txt_crditDownEnp}

TC-PSF-04-04-06 ตรวจสอบการกรอกข้อมูลรหัสนิสิตรับไม่เกิน 8 ตัวอักษร และ ไม่น้อยกว่า 8 
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuID}                      ${txt_stuID2} 

TC-PSF-04-04-07 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuName}                    ${txt_name2}

TC-PSF-04-04-08 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย รับทศนิยมไม่เกิน 2 หลัก และทศนิยมไม่น้อยกว่า 2 หลัก
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuGpaEnp}                  ${txt_gpa2}

TC-PSF-04-04-09 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน รับเลขไม่เกิน 3 หลัก
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${creditPassedEnp}            ${txt_crditPass2}

TC-PSF-04-04-10 ตรวจสอบการกรอกข้อมูลหน่วยกิตลงรับเลขไม่เกิน 3 หลัก
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${creditDownEnp}              ${txt_crditDownEnp2}

TC-PSF-04-04-11 ตรวจสอบการกรอกหากไม่กรอกข้อมูลรหัสนิสิต
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuID}                      ${txt_stuID3} 

TC-PSF-04-04-12 ตรวจสอบการกรอกหากไม่กรอก ข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuName}                    ${txt_name3}

TC-PSF-04-04-13 ตรวจสอบการกรอกหากไม่กรอก ข้อมูลเกรดเฉลี่ย
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuGpaEnp}                  ${txt_gpa3}

TC-PSF-04-04-14 ตรวจสอบการกรอกหากไม่กรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${creditPassedEnp}            ${txt_crditPass3}

TC-PSF-04-04-15 ตรวจสอบการกรอกหากไม่กรอกข้อมูลหน่วยกิตที่ลง
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${creditDownEnp}              ${txt_crditDownEnp3}

TC-PSF-04-04-16 ตรวจสอบการกรอกข้อมูลรหัสนิสิตเป็นตัวอักษรมั่ว
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuID}                      ${txt_stuID4} 
TC-PSF-04-04-17 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุลเป็นตัวอักษรมั่ว
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuName}                    ${txt_name4}

TC-PSF-04-04-18 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ยเป็นตัวอักษรมั่ว
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${stuGpaEnp}                  ${txt_gpa4}

TC-PSF-04-04-19 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่านเป็นตัวอักษรมั่ว
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${creditPassedEnp}            ${txt_crditPass4}

TC-PSF-04-04-20 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลงเป็นตัวอักษรมั่ว
    เปิดหน้าจอ                ${URL_manageStuEnpAdd}           ${web_browser}               
    กรอกข้อมูล                ${creditDownEnp}              ${txt_crditDownEnp4}





















