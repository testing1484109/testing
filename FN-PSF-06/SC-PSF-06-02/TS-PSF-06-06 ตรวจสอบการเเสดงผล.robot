**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL_manageFollowStuEnp}       http://127.0.0.1:8080/followStudentEnp
${web_browser}                  chrome
${btnSunmit}                    ok
${btnFollow}                    xpath=//a[@href="/followStudentEnpReg/9"]
${txt_stuEnpReg}                CS    
${stuEngReg}                    stuEnpReg
***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${stuEngReg}         ${txt}

    
กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุมเข้าสู่ระบบ
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-06-06-01 ตรวจสอบการแสดงผลข้อมูลนิสิตจบไม่ตรงแผน
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}              ${web_browser}
    กดลิงค์รูป                 ${btnFollow}
    กรอกข้อมูล                ${txt_stuEnpReg} 
    กดปุ่ม                    ${btnSunmit} 

