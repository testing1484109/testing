**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL_manageFollowStuEnp}       http://127.0.0.1:8080/followStudentEnp
${web_browser}                  chrome
${txt_name}                     ธันวา
${btnSearch}                    btnsearch
${user_search}                  searchuser

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${user_search}       ${txt}

    
กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุมเข้าสู่ระบบ
    Click Button            ${btn}

***Test Case ***
TC-PSF-06-03-01 ตรวจสอบการแสดงผลข้อมูล รายชื่อนิสิตจบไม่ตรงแผน
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}    ${web_browser}                
    กรอกข้อมูล                ${txt_name}
    กดปุ่ม                    ${btnSearch}  

