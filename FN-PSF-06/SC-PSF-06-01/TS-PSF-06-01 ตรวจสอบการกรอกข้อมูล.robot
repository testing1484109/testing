**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL_manageFollowStuEnp}       http://127.0.0.1:8080/followStudentEnp
${web_browser}                  chrome
${txt_name}                     ธันวา
${txt_name2}                    แสนสุข
${txt_name3}                    58666004		
${txt_name4}                    ธันวา เเสนสุข
${txt_name5}                    58666004ธันวา เเสนสุข 
${txt_name6}               
${txt_name7}                    5555555555555555555555555555555555555555555555555555555555555
${btnSearch}                    btnsearch
${user_search}                  searchuser
***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${user_search}       ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}


***Test Case ***
TC-PSF-06-01-01 ตรวจสอบ การกรอกข้อมูลค้นหาจากชื่อ
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}              ${web_browser}                
    กรอกข้อมูล                ${txt_name}
    กดปุ่ม                    ${btnSearch}  

TC-PSF-06-01-02 ตรวจสอบการกรอกข้อมูลค้นหา จากการใส่เพียงนามสกุล
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}              ${web_browser}                
    กรอกข้อมูล                ${txt_name2}
    กดปุ่ม                    ${btnSearch}  

TC-PSF-06-01-03 ตรวจสอบการกรอกข้อมูลค้นหา จากการใส่เพียงรหัสนิสิต
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}              ${web_browser}                
    กรอกข้อมูล                ${txt_name3}
    กดปุ่ม                    ${btnSearch}  
   
TC-PSF-06-01-04 ตรวจสอบการกรอกข้อมูลค้นหาจาก ชื่อ-นามสกุล
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}              ${web_browser}                
    กรอกข้อมูล                ${txt_name4}
    กดปุ่ม                    ${btnSearch} 

TC-PSF-06-01-05 ตรวจสอบการกรอกข้อมูลค้นหา จาก ชื่อ-นามสกุล รหัสนิสิต
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}              ${web_browser}                
    กรอกข้อมูล                ${txt_name5}
    กดปุ่ม                    ${btnSearch} 
TC-PSF-06-01-06 ตรวจสอบการกรอกข้อมูลค้นหา โดยไม่กรอกอะไรเลย
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}              ${web_browser}                
    กรอกข้อมูล                ${txt_name6}
    กดปุ่ม                    ${btnSearch}
TC-PSF-06-01-07 ตรวจสอบการกรอกข้อมูลค้นหา รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}              ${web_browser}                
    กรอกข้อมูล                ${txt_name7}
    กดปุ่ม                    ${btnSearch}

