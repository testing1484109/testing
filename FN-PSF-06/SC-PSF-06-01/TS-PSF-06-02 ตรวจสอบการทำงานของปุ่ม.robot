**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL_manageFollowStuEnp}       http://127.0.0.1:8080/followStudentEnp
${web_browser}                  chrome
${btnSearch}                    btnsearch
${btnFollow}                    xpath=//a[@href="/followStudentEnpReg/9"]

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กรอกข้อมูล
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-06-02-01 ตรวจสอบการกดปุ่มค้นหา
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}              ${web_browser}                
    กดปุ่ม                    ${btnSearch}    

TC-PSF-06-02-02 ตรวจสอบการกดปุ่มติดตาม
    เปิดหน้าจอ                ${URL_manageFollowStuEnp}              ${web_browser}
    กดลิงค์รูป                 ${btnFollow}
