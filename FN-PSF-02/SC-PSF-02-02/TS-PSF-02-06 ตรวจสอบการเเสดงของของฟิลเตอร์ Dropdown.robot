**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***

${role}           อาจารย์
${btnAdd}         btnAdd
${URL_Add}        http://127.0.0.1:8080/manageUser_add
${URL_manage}     http://127.0.0.1:8080/manageUser
${web_browser}    firefox
${dropdown}       statusUser

***Keyword***
เปิดหน้าจอ
    [Arguments]         ${link_web}    ${web_browser}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser        ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]         ${var}    ${txt}
    [Documentation]     กรอกข้อมูล
    Input Text          ${var}    ${txt}

กดปุ่ม
    [Arguments]         ${btn}
    [Documentation]     กดปุ่ม
    Click Button        ${btn}

กด Dropdown
    [Arguments]         ${dropdown}    ${role}
    [Documentation]     ใช้กดDropdown
    Select From List By Label    ${dropdown}    ${role}

***Test Case ***
TC-PSF-02-06-01 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลตำแหน่ง
    เปิดหน้าจอ     ${URL_Add}    ${web_browser}
    กด Dropdown    ${dropdown}      ${role}



