**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${txt_user}             thidarat
${txt_password}         123456
${txt_name}             thidarat
${role}                 อาจารย์            
${user}                 username
${password}             password
${nameuser}             nameuser
${btnSave}              ok
${btnCancle}            cancel
${URL_Add}              http://127.0.0.1:8080/manageUser_add
${URL_manage}           http://127.0.0.1:8080/manageUser
${web_browser}          chrome
${dropdown}             statusUser

***Keyword***
เปิดหน้าจอ
    [Arguments]                         ${link_web}         ${web_browser}
    [Documentation]                     ใช้เปิดหน้าจอ
    Open Browser                        ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]                         ${var}              ${txt}
    [Documentation]                     กรอกข้อมูล
    Input Text                          ${var}              ${txt} 

กดปุ่ม
    [Arguments]                         ${btn}
    [Documentation]                     กดปุ่ม
    Click Button                        ${btn}

กด Dropdown 
    [Arguments]                         ${dropdown}         ${role}
    [Documentation]                     ใช้กดDropdown
    Select From List By Label           ${dropdown}         ${role}  

***Test Case ***
TC-PSF-02-05-01 ตรวจสอบการกดปุ่มบันทึก
    เปิดหน้าจอ                            ${URL_manage}       ${web_browser}
    เปิดหน้าจอ                            ${URL_Add}          ${web_browser}                
    กรอกข้อมูล                            ${user}             ${txt_user}
    กรอกข้อมูล                            ${password}         ${txt_password}  
    กรอกข้อมูล                            ${nameuser}         ${txt_name}
    กด Dropdown                         ${dropdown}         ${role}
    กดปุ่ม                                ${btnSave}
TC-PSF-02-05-02 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ                            ${URL_manage}       ${web_browser}
    เปิดหน้าจอ                            ${URL_Add}          ${web_browser}                
    กด Dropdown                         ${dropdown}         ${role}
    กดปุ่ม                                ${btnCancle}

