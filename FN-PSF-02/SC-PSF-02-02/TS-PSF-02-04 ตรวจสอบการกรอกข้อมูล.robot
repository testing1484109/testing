**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${txt_user}     sakdaa
${txt_user2}    sakdaa123
${txt_user3}
${txt_user4}        er21$5ยน๗xXa^
${txt_password}     123456
${txt_password2}    123
${txt_password3}
${txt_password4}    er21$5ยน๗xXa^
${txt_name}         sakdaa5678
${txt_name2}        ศักดา อ่อนพรม 555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555
${txt_name3}
${txt_name4}        er21$5ยน๗xXa^
${role}             อาจาร์ย
${user}             username
${password}         password
${nameuser}         nameuser
${btnAdd}           btnAdd
${btnSave}          ok
${btnCancle}        cancel

${URL_manageAdd}    http://127.0.0.1:8080/manageUser_add
${URL_manage}       http://127.0.0.1:8080/manageUser
${web_browser}      firefox

***Keyword***
เปิดหน้าจอ
    [Arguments]    ${link_web}    ${web_browser}
    [Documentation]     ใช้เปิดหน้าจอ
    Open Browser    ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]    ${var}    ${txt}
    [Documentation]      กรอกข้อมูล
    Input Text    ${var}    ${txt}

กดปุ่ม
    [Arguments]    ${btn}
    [Documentation]      กดปุ่ม
    Click Button    ${btn}

***Test Case ***

TC-PSF-02-04-01 ตรวจสอบการกรอกข้อมูลชื่อผู้ใช้
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดปุ่ม        ${btnAdd}
    กรอกข้อมูล    ${user}             ${txt_user}

TC-PSF-02-04-02 ตรวจสอบ การกรอกข้อมูลรหัสผ่าน
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดปุ่ม        ${btnAdd}
    กรอกข้อมูล    ${password}         ${txt_password}

TC-PSF-02-04-03 ตรวจสอบ การกรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ    ${URL_manageAdd}    ${web_browser}
    กรอกข้อมูล    ${nameuser}         ${txt_name}

TC-PSF-02-04-04 ตรวจสอบ การกรอกข้อมูลชื่อผู้ใช้รับไม่เกิน 6 ตัวอักษร และไม่น้อยกว่า 6 ตัวอักษร
    เปิดหน้าจอ    ${URL_manageAdd}    ${web_browser}
    กรอกข้อมูล    ${user}             ${txt_user2}

TC-PSF-02-04-05 ตรวจสอบ การกรอกข้อมูลรหัสผ่านรับไม่ เกิน 6 ตัวอักษร และไม่น้อยกว่า 6 ตัวอักษร
    เปิดหน้าจอ    ${URL_manageAdd}    ${web_browser}
    กรอกข้อมูล    ${password}         ${txt_password2}

TC-PSF-02-04-06 ตรวจสอบ การกรอกข้อมูลชื่อ-นามสกุล รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอ    ${URL_manageAdd}    ${web_browser}
    กรอกข้อมูล    ${nameuser}         ${txt_name2}

TC-PSF-02-04-07 ตรวจสอบ การหากไม่กรอกข้อมูลชื่อผู้ใช้
    เปิดหน้าจอ    ${URL_manageAdd}    ${web_browser}
    กรอกข้อมูล    ${user}             ${txt_user3}

TC-PSF-02-04-08 ตรวจสอบ การหากไม่กรอกข้อมูลรหัสผ่าน
    เปิดหน้าจอ    ${URL_manageAdd}    ${web_browser}
    กรอกข้อมูล    ${password}         ${txt_password3}

TC-PSF-02-04-09 ตรวจสอบ การหากไม่กรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ    ${URL_manageAdd}    ${web_browser}
    กรอกข้อมูล    ${nameuser}         ${txt_name3}

TC-PSF-02-04-10 ตรวจสอบ การหากกรอกข้อมูลชื่อผู้ใช้เป็นตัวอักษรมั่ว
    เปิดหน้าจอ    ${URL_manageAdd}    ${web_browser}
    กรอกข้อมูล    ${user}             ${txt_user4}
TC-PSF-02-04-11 ตรวจสอบ การกรอกข้อมูลรหัสผ่านเป็นตัวอักษรมั่ว
    เปิดหน้าจอ    ${URL_manageAdd}    ${web_browser}
    กรอกข้อมูล    ${password}         ${txt_password4}
TC-PSF-02-04-12 ตรวจสอบ การกรอกข้อมูลชื่อ-นามสกุลเป็น ตัวอักษรมั่ว
    เปิดหน้าจอ    ${URL_manageAdd}    ${web_browser}
    กรอกข้อมูล    ${user}             ${txt_user4}


