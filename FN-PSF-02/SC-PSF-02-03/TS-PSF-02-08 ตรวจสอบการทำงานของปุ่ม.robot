**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***

${txt_name}             ประยวย
${role}                 อาจารย์            
${user}                 username
${password}             password
${nameuser}             nameuser
${btnEdit}              xpath=//a[@href="/manageUser_edit/1"]
${btnhDel}              xpath=//a[@href="/delUser/1"]
${btnSave}              ok
${btnCancle}            cancel
${URL}                  http://127.0.0.1:8080/manageUser_add
${URL_manage}           http://127.0.0.1:8080/manageUser
${web_browser}          chrome
${dropdown}             statusUser

***Keyword***
เปิดหน้าจอ
    [Arguments]                         ${link_web}         ${web_browser}
    [Documentation]                     ใช้เปิดหน้าจอ
    Open Browser                        ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]                         ${var}                 ${txt}
    [Documentation]                     กรอกข้อมูล
    Input Text                          ${var}                 ${txt} 

กดปุ่ม
    [Arguments]                         ${btn}
    [Documentation]                     กดปุ่ม
    Click Button                        ${btn}

กด Dropdown 
    [Arguments]                         ${dropdown}         ${role}
    [Documentation]                     ใช้กดDropdown
    Select From List By Label           ${dropdown}         ${role}  

กดลิงค์รูป
    [Arguments]                         ${link_page}
    [Documentation]                     ใช้กดลิงค์
    Click Link                          ${link_page}
    
***Test Case ***
TC-PSF-02-08-01 ตรวจสอบการกดปุ่มบันทึก
    เปิดหน้าจอ                            ${URL_manage}       ${web_browser}   
    กดลิงค์รูป                             ${btnEdit}             
    กรอกข้อมูล                            ${nameuser}         ${txt_name}  
    กดปุ่ม                                ${btnSave}

TC-PSF-02-08-02 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ                            ${URL_manage}       ${web_browser}
    กดลิงค์รูป                             ${btnEdit}                      
    กดปุ่ม                                ${btnCancle}

