**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${txt_user}     pichet01
${txt_user2}    pichet01
${txt_user3}
${txt_user4}        er21$5ยน๗xXa^
${txt_password}     1
${txt_password2}    123
${txt_password3}
${txt_password4}    er21$5ยน๗xXa^
${txt_name}         ดร พิเชษ วะยะลุน
${txt_name2}        2222222222222222222222222222222222222222222222222222
${txt_name3}
${txt_name4}      er21$5ยน๗xXa^
${role}           อาจาร์ย
${user}           username
${password}       password
${nameuser}       nameuser
${btnSave}        ok
${btnCancle}      cancel
${URL_manage}     http://127.0.0.1:8080/manageUser
${web_browser}    chrome
${btnEdit}        xpath=//a[@href="/manageUser_edit/1"]
${btnDel}         xpath=//a[@href="/delUser/1"]

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}    ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${var}    ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กด Dropdown
    [Arguments]             ${dropdown}    ${role}
    [Documentation]          ใช้กดDropdown
    Select From List By Label    ${dropdown}    ${role}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-02-07-01 ตรวจสอบการกรอกข้อมูลชื่อผู้ใช้
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${user}          ${txt_user}

TC-PSF-02-07-02 ตรวจสอบการกรอกข้อมูลรหัสผ่าน
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${password}      ${txt_password}

TC-PSF-02-07-03 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${nameuser}      ${txt_name}

TC-PSF-02-07-04 ตรวจสอบการกรอกข้อมูลชื่อผู้ใช้ รับไม่เกิน 6 ตัวอักษร และไม่ น้อยกว่า 6 ตัวอักษร
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${user}          ${txt_user2}

TC-PSF-02-07-05 ตรวจสอบการกรอกข้อมูล รหัสผ่าน รับไม่เกิน 6 ตัวอักษร และไม่น้อยกว่า 6 ตัวอักษร
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${password}      ${txt_password2}

TC-PSF-02-07-06 ตรวจสอบการกรอกข้อมูลชื่อ- นามสกุล รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${nameuser}      ${txt_name2}

TC-PSF-02-07-07 ตรวจสอบ การหากไม่กรอกข้อมูลชื่อผู้ใช้
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${user}          ${txt_user3}

TC-PSF-02-07-08 ตรวจสอบ การหากไม่กรอกข้อมูลรหัสผ่าน
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${password}      ${txt_password3}

TC-PSF-02-07-09 ตรวจสอบ การหากไม่กรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${nameuser}      ${txt_name3}

TC-PSF-02-07-10 ตรวจสอบ การหากกรอกข้อมูลชื่อผู้ใช้เป็น ตัวอักษรมั่ว
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${user}          ${txt_user4}

TC-PSF-02-07-11 ตรวจสอบ การหากกรอกข้อมูลรหัสผ่านเป็น ตัวอักษรมั่ว
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${password}      ${txt_password4}

TC-PSF-02-07-12 ตรวจสอบ การหากกรอกข้อมูลชื่อ-นามสกุลเป็นตัวอักษรมั่ว
    เปิดหน้าจอ    ${URL_manage}    ${web_browser}
    กดลิงค์รูป     ${btnEdit}
    กรอกข้อมูล    ${user}          ${txt_user4}


