**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL_manage}           http://127.0.0.1:8080/manageUser
${web_browser}          firefox
${btnEdit1}              xpath=//a[@href="/manageUser_edit/1"]
${btnEdit2}              xpath=//a[@href="/manageUser_edit/2"]
${btnEdit3}              xpath=//a[@href="/manageUser_edit/3"]

***Keyword***
เปิดหน้าจอ
    [Arguments]                         ${link_web}         ${web_browser}
    [Documentation]                     ใช้เปิดหน้าจอ
    Open Browser                        ${link_web}         ${web_browser}

กดลิงค์รูป
    [Arguments]                         ${link_page}
    [Documentation]                     ใช้กดลิงค์
    Click Link                          ${link_page}

***Test Case ***
TC-PSF-02-09-01 ตรวจสอบการแสดงผลข้อมูลชื่อ ผู้ใช้ที่เลือกจากตาราง
    เปิดหน้าจอ                            ${URL_manage}       ${web_browser} 
    กดลิงค์รูป                             ${btnEdit1}

TC-PSF-02-09-02 ตรวจสอบการแสดงผลข้อมูลรหัสผ่านที่เลือกจากตาราง
    เปิดหน้าจอ                            ${URL_manage}       ${web_browser} 
    กดลิงค์รูป                             ${btnEdit2}
TC-PSF-02-09-03 ตรวจสอบการแสดงผลข้อมูลชื่อ-นามสกุลที่เลือกจากตาราง
    เปิดหน้าจอ                            ${URL_manage}       ${web_browser} 
    กดลิงค์รูป                             ${btnEdit3}


