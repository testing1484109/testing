**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***

${role}                 อาจารย์            
${btnAdd}               btnAdd
${URL_manage}           http://127.0.0.1:8080/manageUser
${web_browser}          firefox
${dropdown}             statusUser
${btnEdit}              xpath=//a[@href="/manageUser_edit/1"]
***Keyword***
เปิดหน้าจอ
    [Arguments]                         ${link_web}         ${web_browser}
    [Documentation]                     ใช้เปิดหน้าจอ
    Open Browser                        ${link_web}         ${web_browser}


กด Dropdown 
    [Arguments]                         ${dropdown}         ${role}
    [Documentation]                     ใช้กดDropdown
    Select From List By Label           ${dropdown}         ${role}  

กดลิงค์รูป
    [Arguments]                         ${link_page}
    [Documentation]                     ใช้กดลิงค์
    Click Link                          ${link_page}

***Test Case ***
TC-PSF-02-06-01 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลตำแหน่ง
    เปิดหน้าจอ                            ${URL_manage}       ${web_browser} 
    กดลิงค์รูป                             ${btnEdit}           
    กด Dropdown                         ${dropdown}         ${role}



