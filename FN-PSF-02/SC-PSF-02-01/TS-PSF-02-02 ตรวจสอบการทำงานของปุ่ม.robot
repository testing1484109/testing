**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL}                  http://127.0.0.1:8080/manageUser
${btnSearch}            btnsearch
${btnAdd}               btnAdd
${btnEdit}              xpath=//a[@href="/manageUser_edit/1"]
${btnhDel}              xpath=//a[@href="/delUser/1"]
${web_browser}          firefox

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กรอกข้อมูล
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-02-02-01 ตรวจสอบการกดปุ่มค้นหา
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กดปุ่ม                    ${btnSearch}    

TC-PSF-02-02-02 ตรวจสอบการกดปุ่มเพิ่มข้อมูลผู้ใช้งาน
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กดปุ่ม                    ${btnAdd}  

TC-PSF-02-02-03 ตรวจสอบการกดลิงค์แก้ไขข้อมูลผู้ใช้งาน
    เปิดหน้าจอ                ${URL}              ${web_browser}
    กดลิงค์รูป                 ${btnEdit}

TC-PSF-02-02-04 ตรวจสอบการกดลิงค์ลบข้อมูลผู้ใช้งาน
    เปิดหน้าจอ                ${URL}              ${web_browser}
    กดลิงค์รูป                 ${btnEdit}
