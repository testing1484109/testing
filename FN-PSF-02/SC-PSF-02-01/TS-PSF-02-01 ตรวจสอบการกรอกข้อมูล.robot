**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${user_search}          search
${txt_name1}            pichet
${txt_name2}            พิเชษ
${txt_name3}            วะยะลุน
${txt_name4}            2222222222222222222222222222222222222222222222222222
${URL}                  http://127.0.0.1:8080/manageUser
${btnSearch}            btnsearch
${btnAdd}               btnAdd
${PathEdit}             xpath=//a[@href="/manageUser_edit/1"]
${PathDel}              xpath=//a[@href="/delUser/1"]
${web_browser}          firefox

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${txt}
    [Documentation]         กรอกข้อมูลผู้ใช้งาน
    Input Text              ${user_search}       ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กรอกข้อมูลผู้ใช้งาน
    Click Button            ${btn}


***Test Case ***
TC-PSF-02-01-01ตรวจสอบการกรอกข้อมูลค้นหาจากชื่อผู้ใช้
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กรอกข้อมูล                ${txt_name1}
    กดปุ่ม                    ${btnSearch}  
TC-PSF-02-01-02 ตรวจสอบการกรอกข้อมูลค้นหาจากชื่อ
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กรอกข้อมูล                ${txt_name2}
    กดปุ่ม                    ${btnSearch}  
TC-PSF-02-01-03 ตรวจสอบการกรอกข้อมูลค้นหาจากนามสกุล
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กรอกข้อมูล                ${txt_name3}
    กดปุ่ม                    ${btnSearch}  
   
TC-PSF-02-01-04 ตรวจสอบการกรอกข้อมูลค้นหา รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กรอกข้อมูล                ${txt_name4}
    กดปุ่ม                    ${btnSearch} 
