**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${txt_user1}            pichet
${txt_user2}            somnuk	
${txt_password}         123456
${email}                email
${password}             password
${URL}                  http://127.0.0.1:8080
${web_browser}          firefox
${btn}                  btnok

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}              ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${var}              ${txt}
    
กดปุ่ม
    [Arguments]             ${btnsearch}
    [Documentation]         กดปุมเข้าสู่ระบบ
    Click Button            ${btn}

***Test Case ***
TC-PSF-02-03-01 ตรวจสอบการแสดงผลข้อมูลรายชื่อผู้ใช้งานสถานะอาจารย์
    เปิดหน้าจอ                ${URL}              ${web_browser}
    กรอกข้อมูล                ${email}            ${txt_user1}                
    กรอกข้อมูล                ${password}         ${txt_password}
    กดปุ่ม                    ${btn}
TC-PSF-02-03-02 ตรวจสอบ การแสดงผลข้อมูลรายชื่อ ผู้ใช้งานสถานะเจ้าหน้าที่
    เปิดหน้าจอ                ${URL}              ${web_browser}
    กรอกข้อมูล                ${email}            ${txt_user2}                
    กรอกข้อมูล                ${password}         ${txt_password}
    กดปุ่ม                    ${btn}

