**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${btnSave}              ok
${btnCancle}            cancel
${URL_manage}           http://127.0.0.1:8080/manageUser
${btnDel}               xpath=//a[@href="/delUser/2"]
${web_browser}          firefox

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${user_name}
    [Documentation]         กรอกข้อมูล
    Input Text              ${user_search}       ${user_name}
    Click Button            ${btn}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-02-11-01 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ                ${URL_manage}              ${web_browser} 
    กดลิงค์รูป                 ${btnDel}
    กดปุ่ม                    ${btnCancle}      
       
TC-PSF-02-11-02 ตรวจสอบการกดปุ่มตกลง
    เปิดหน้าจอ                ${URL_manage}              ${web_browser} 
    กดลิงค์รูป                 ${btnDel}
    กดปุ่ม                    ${btnSave}   

