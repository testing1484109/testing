**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${txt_user}             somnuk
${txt_password}         123456
${URL}                  http://127.0.0.1:8080
${email}                email
${password}             password
${web_browser}          firefox
${btn}                  btnok

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}              ${txt}
    [Documentation]         กรอกข้อมูลผ
    Input Text              ${var}              ${txt}
      
กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุมเข้าสู่ระบบ
    Click Button            ${btn}


***Test Case ***

TC-PSF-01-02-01 ตรวจสอบการกดปุ่มเข้าสู่ระบบ
    เปิดหน้าจอ                ${URL}              ${web_browser}
    กรอกข้อมูล                ${email}            ${txt_user}                
    กรอกข้อมูล                ${password}         ${txt_password}
    กดปุ่ม                    ${btn}



