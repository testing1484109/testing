**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${txt_user1}            pichet
${txt_user2}            pichet01
${txt_user3}            piche
${txt_user4}            somnuk	
${txt_user5}            thidarat	
${txt_password1}        123456
${txt_password2}        1234567
${txt_password3}        12345
${txt_password4}        
${email}                email
${password}             password
${URL}                  http://127.0.0.1:8080
${web_browser}          chrome
${btn}                  btnok

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}              ${txt}
    [Documentation]         กรอกข้อมูลผ
    Input Text              ${var}              ${txt}
    
กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุมเข้าสู่ระบบ
    Click Button            ${btn}


***Test Case ***
TC-PSF-01-01-01 ตรวจสอบการกรอกข้อมูล Username รับไม่เกินจำนวน 6 ตัวอักษรและไม่น้อยกว่า 6 ตัวอักษร
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กรอกข้อมูล                ${email}            ${txt_user1}

TC-PSF-01-01-02 ตรวจสอบการกรอกข้อมูล Username รับเกินจำนวน 6 ตัวอักษร
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กรอกข้อมูล                ${email}            ${txt_user2}

TC-PSF-01-01-03 ตรวจสอบการกรอกข้อมูล Username รับน้อยกว่าจำนวน 6 ตัวอักษร
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กรอกข้อมูล                ${email}            ${txt_user3}

TC-PSF-01-01-04 ตรวจสอบการกรอกข้อมูล Password รับไม่เกินจำนวน 6 ตัวอักษรและไม่น้อยกว่า 6 ตัวอักษร
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กรอกข้อมูล                ${password}         ${txt_password1}

TC-PSF-01-01-05 ตรวจสอบการกรอกข้อมูล Password รับเกินจำนวน 6 ตัวอักษร
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กรอกข้อมูล                ${password}         ${txt_password2}

TC-PSF-01-01-06 ตรวจสอบการกรอกข้อมูล Password รับน้อยกว่าจำนวน 6 ตัวอักษร
    เปิดหน้าจอ                ${URL}              ${web_browser}                
    กรอกข้อมูล                ${password}         ${txt_password3}

TC-PSF-01-01-07 ตรวจสอบการกรอกข้อมูล Password รับข้อความที่กรอกไม่แสดงตัวอักษร
    เปิดหน้าจอ                ${URL}              ${web_browser}               
    กรอกข้อมูล                ${password}         ${txt_password4}

TC-PSF-01-01-08 ตรวจสอบการเข้าสู้ระบบสำหรับผู้ใช้สถานะอาจารย์
    เปิดหน้าจอ                ${URL}              ${web_browser}
    กรอกข้อมูล                ${email}            ${txt_user1}                
    กรอกข้อมูล                ${password}         ${txt_password1}

TC-PSF-01-01-09 ตรวจสอบการเข้าสู้ระบบสำหรับผู้ใช้สถานะเจ้าหน้าที่
    เปิดหน้าจอ                ${URL}              ${web_browser}
    กรอกข้อมูล                ${email}            ${txt_user4}                
    กรอกข้อมูล                ${password}         ${txt_password1}
    กดปุ่ม                    ${btn}

TC-PSF-01-01-10 ตรวจสอบการเข้าสู้ระบบสำหรับผู้ใช้ที่ไม่มีสิทธิเข้าสู่ระบบ
    เปิดหน้าจอ                ${URL}              ${web_browser}
    กรอกข้อมูล                ${email}            ${txt_user5}                
    กรอกข้อมูล                ${password}         ${txt_password1}
    กดปุ่ม                    ${btn}


