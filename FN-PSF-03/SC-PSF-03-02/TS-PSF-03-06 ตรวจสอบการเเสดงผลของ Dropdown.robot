**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuAdd}        http://127.0.0.1:8080/manageStudentCon_add
${web_browser}             chrome

${txt_stuLevelCon}         ตรี พิเศษ
${txt_stuStatusCon}        10
${txt_considertime}        2
${txt_considerCon}         โปรต่ำ

${stuLevelCon}             stuLevelCon
${stuStatusCon}            stuStatusCon
${considertime}            considertime
${considerCon}             considerCon

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กด Dropdown
    [Arguments]                 ${dropdown}     ${txt}
    [Documentation]             ใช้กดDropdown
    Select From List By Label   ${dropdown}    ${txt}

***Test Case ***
TC-PSF-03-06-01 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลระดับ
    เปิดหน้าจอ                ${URL_manageStuAdd}           ${web_browser}
    กด Dropdown             ${stuLevelCon}                ${txt_stuLevelCon}

TC-PSF-03-06-02 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลสถานภาพ
    เปิดหน้าจอ                ${URL_manageStuAdd}           ${web_browser}
    กด Dropdown             ${stuStatusCon}               ${txt_stuStatusCon}

TC-PSF-03-06-03 ตรวจสอบการแสดงผลฟิ ลเตอร์ Dropdown ข้อมูลรอการพิจารณา
    เปิดหน้าจอ                ${URL_manageStuAdd}           ${web_browser}
    กด Dropdown             ${considerCon}                ${txt_considerCon}
TC-PSF-03-06-04 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลครั้งที่
    เปิดหน้าจอ                ${URL_manageStuAdd}           ${web_browser}
    กด Dropdown             ${considertime}               ${txt_considertime}




























