**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuAdd}        http://127.0.0.1:8080/manageStudentCon_add
${web_browser}             chrome

${txt_stuID}               586660044
${txt_name}                ธันวา เเสนสุข
${txt_gpa}                 2.00
${txt_crditPass}           3.00
${txt_crditDownCon}        1.50
${txt_stuLevelCon}         ตรี พิเศษ
${txt_stuStatusCon}        10
${txt_considertime}        2
${txt_considerCon}         โปรต่ำ

${btnSave}                 ok
${btnCancle}               cancel
${stuID}                   studentIDCon
${stuName}                 nameStuCon
${stuLevelCon}             stuLevelCon
${stuGpaCon}               stuGpaCon
${stuStatusCon}            stuStatusCon
${creditPassedCon}         creditPassedCon
${creditDownCon}           creditDownCon
${considertime}            considertime
${considerCon}             considerCon

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}          ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${var}          ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กด Dropdown
    [Arguments]                 ${dropdown}     ${txt}
    [Documentation]             ใช้กดDropdown
    Select From List By Label   ${dropdown}    ${txt}

***Test Case ***
TC-PSF-03-05-01 ตรวจสอบการกดปุ่มบันทึก
    เปิดหน้าจอ                ${URL_manageStuAdd}           ${web_browser}
    กรอกข้อมูล                ${stuID}                      ${txt_stuID}
    กรอกข้อมูล                ${stuName}                    ${txt_name}
    กด Dropdown             ${stuLevelCon}                ${txt_stuLevelCon}
    กรอกข้อมูล                ${stuGpaCon}                  ${txt_gpa}
    กด Dropdown             ${stuStatusCon}               ${txt_stuStatusCon}
    กรอกข้อมูล                ${creditPassedCon}            ${txt_crditPass}
    กรอกข้อมูล                ${creditDownCon}              ${txt_crditDownCon}
    กด Dropdown             ${considertime}               ${txt_considertime}
    กด Dropdown             ${considerCon}                ${txt_considerCon}
    กดปุ่ม                    ${btnSave} 

TC-PSF-03-05-02 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอ                ${URL_manageStuAdd}           ${web_browser}
    กรอกข้อมูล                ${stuID}                      ${txt_stuID}
    กรอกข้อมูล                ${stuName}                    ${txt_name}
    กด Dropdown             ${stuLevelCon}                ${txt_stuLevelCon}
    กรอกข้อมูล                ${stuGpaCon}                  ${txt_gpa}
    กด Dropdown             ${stuStatusCon}               ${txt_stuStatusCon}
    กรอกข้อมูล                ${creditPassedCon}            ${txt_crditPass}
    กรอกข้อมูล                ${creditDownCon}              ${txt_crditDownCon}
    กด Dropdown             ${considertime}               ${txt_considertime}
    กด Dropdown             ${considerCon}                ${txt_considerCon}              
    กดปุ่ม                    ${btnCancle} 





























