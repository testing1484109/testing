**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL_manageStu}           http://127.0.0.1:8080/manageStudentConsider
${web_browser}             chrome
${txt_name}                สมพง
${btnSearch}               btnsearch
${user_search}             searchuser

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${user_search}       ${txt}

    
กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุมเข้าสู่ระบบ
    Click Button            ${btn}

***Test Case ***
TC-PSF-03-03-01 ตรวจสอบการแสดงผลข้อมูล รายชื่อนิสิตรอการพินิจ
    เปิดหน้าจอ                ${URL_manageStu}    ${web_browser}                
    กรอกข้อมูล                ${txt_name}
    กดปุ่ม                    ${btnSearch}  

