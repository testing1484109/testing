**Settings**
Library             SeleniumLibrary
Test Teardown       Close Browser

***Variables***
${URL_manageStu}        http://127.0.0.1:8080/manageStudentConsider
${web_browser}          chrome
${btnSearch}            btnsearch
${btnAdd}               btnAdd
${btnEdit}              xpath=//a[@href="/manageCon_edit/2"]
${btnhDel}              xpath=//a[@href="/delCon/2"]

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}         ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}         ${web_browser}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กรอกข้อมูล
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-03-02-01 ตรวจสอบการกดปุ่มค้นหา
    เปิดหน้าจอ                ${URL_manageStu}              ${web_browser}                
    กดปุ่ม                    ${btnSearch}    

TC-PSF-03-02-02 ตรวจสอบการกดปุ่มเพิ่มข้อมูล
    เปิดหน้าจอ                ${URL_manageStu}              ${web_browser}                
    กดปุ่ม                    ${btnAdd}  

TC-PSF-03-02-03 ตรวจสอบการกดปุ่มแก้ไข
    เปิดหน้าจอ                ${URL_manageStu}              ${web_browser}
    กดลิงค์รูป                 ${btnEdit}

TC-PSF-03-02-04 ตรวจสอบการกดปุ่ มลบ
    เปิดหน้าจอ                ${URL_manageStu}              ${web_browser}
    กดลิงค์รูป                 ${btnEdit}
