**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuCon}        http://127.0.0.1:8080/manageStudentConsider
${web_browser}             chome

${txt_stuID}               586660044
${txt_name}                ธิดารัตน์  โคตรพัฒน์
${txt_gpa}                 2.00
${txt_crditPass}           3.00
${txt_crditDownCon}        1.50
${txt_stuLevelCon}         ตรี พิเศษ
${txt_stuStatusCon}        10
${txt_considertime}        2
${txt_considerCon}         โปรต่ำ

${btnSave}                 ok
${btnCancle}               cancel
${btnEdit}                 xpath=//a[@href="/manageCon_edit/20"]

${stuID}                   studentIDCon
${stuName}                 nameStuCon
${stuLevelCon}             stuLevelCon
${stuGpaCon}               stuGpaCon
${stuStatusCon}            stuStatusCon
${creditPassedCon}         creditPassedCon
${creditDownCon}           creditDownCon
${considertime}            considertime
${considerCon}             considerCon

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}          ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${var}          ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กด Dropdown
    [Arguments]                 ${dropdown}     ${txt}
    [Documentation]             ใช้กดDropdown
    Select From List By Label   ${dropdown}     ${txt}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-03-09-01 ตรวจสอบการแสดงผลข้อมูลรหัสนิสิต
   เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
   กดลิงค์รูป                 ${btnEdit}
   กรอกข้อมูล                ${stuID}                      ${txt_stuID}
   กดปุ่ม                    ${btnSave} 

TC-PSF-03-09-02 ตรวจสอบการแสดงผลข้อมูลชื่อ-นามสกุล
   เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
   กดลิงค์รูป                 ${btnEdit}
   กรอกข้อมูล                ${stuName}                    ${txt_name}
   กดปุ่ม                    ${btnSave} 

TC-PSF-03-09-03 ตรวจสอบการแสดงผลข้อมูล เกรดเฉลี่ย
   เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
   กดลิงค์รูป                 ${btnEdit}
   กรอกข้อมูล                ${stuGpaCon}                  ${txt_gpa}
   กดปุ่ม                    ${btnSave} 

TC-PSF-03-09-04 ตรวจสอบการแสดงผลข้อมูล หน่วยกิตที่ผ่าน
   เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
   กดลิงค์รูป                 ${btnEdit}
   กรอกข้อมูล                ${creditPassedCon}            ${txt_crditPass}
   กดปุ่ม                    ${btnSave} 

TC-PSF-03-09-05 ตรวจสอบการแสดงข้อมูลหน่วย กิตที่ลง
   เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
   กดลิงค์รูป                 ${btnEdit}
   กรอกข้อมูล                ${creditDownCon}              ${txt_crditDownCon}
   กดปุ่ม                    ${btnSave} 






























