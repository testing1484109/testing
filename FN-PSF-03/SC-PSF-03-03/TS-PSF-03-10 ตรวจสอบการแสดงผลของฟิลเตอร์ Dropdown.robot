**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuCon}        http://127.0.0.1:8080/manageStudentConsider
${web_browser}             chrome

${txt_stuLevelCon}         ตรี พิเศษ
${txt_stuStatusCon}        10
${txt_considertime}        2
${txt_considerCon}         โปรต่ำ

${btnSave}                 ok
${btnCancle}               cancel
${btnEdit}                 xpath=//a[@href="/manageCon_edit/2"]

${stuID}                   studentIDCon
${stuName}                 nameStuCon
${stuLevelCon}             stuLevelCon
${stuGpaCon}               stuGpaCon
${stuStatusCon}            stuStatusCon
${creditPassedCon}         creditPassedCon
${creditDownCon}           creditDownCon
${considertime}            considertime
${considerCon}             considerCon

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}          ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${var}          ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กด Dropdown
    [Arguments]                 ${dropdown}     ${txt}
    [Documentation]             ใช้กดDropdown
    Select From List By Label   ${dropdown}    ${txt}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***
TC-PSF-03-10-01 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลระดับ
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}   
    กด Dropdown             ${stuLevelCon}                ${txt_stuLevelCon}
    กดปุ่ม                    ${btnSave} 

TC-PSF-03-10-02 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลสถานภาพ
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}   
    กด Dropdown             ${stuStatusCon}               ${txt_stuStatusCon}
    กดปุ่ม                    ${btnSave} 

TC-PSF-03-10-03 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลรอการพิจารณา
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}   
    กด Dropdown             ${considerCon}                ${txt_considerCon}
    กดปุ่ม                    ${btnSave} 

TC-PSF-03-10-04 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลครั้งที่
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}   
    กด Dropdown             ${considertime}               ${txt_considertime}
    กดปุ่ม                    ${btnSave} 

