**Settings**
Library          SeleniumLibrary
Test Teardown    Close Browser

***Variables***
${URL_manageStuCon}        http://127.0.0.1:8080/manageStudentConsider
${web_browser}             chrome

${txt_stuID}               586660044
${txt_stuID2}              63160200
${txt_stuID3}  
${txt_stuID4}              $%^&*12354@jfof;    

${txt_name}                ธันวา เเสนสุข
${txt_name2}               ธิดารัตน์ โคตรพัฒน์5555555555555555555555555555555555555555555555555
${txt_name3}               
${txt_name4}               $%^&*12354@jfof;

${txt_gpa}                 2.00
${txt_gpa2}                1.11111111
${txt_gpa3}                
${txt_gpa4}                $%^&*12354@jfof;

${txt_crditPass}           3.00
${txt_crditPass2}          3.00
${txt_crditPass3}          
${txt_crditPass4}          $%^&*12354@jfof;

${txt_crditDownCon}        1.50
${txt_crditDownCon2}       230.0
${txt_crditDownCon3}       
${txt_crditDownCon4}       $%^&*12354@jfof;

${btnAdd}                  btnAdd
${btnEdit}                 xpath=//a[@href="/manageCon_edit/2"]
${btnDel}                  xpath=//a[@href="/delCon/2"]     
${btnSave}                 ok
${btnCancle}               cancel

${stuID}                   studentIDCon
${stuName}                 nameStuCon
${stuLevelCon}             stuLevelCon
${stuGpaCon}               stuGpaCon
${stuStatusCon}            stuStatusCon
${creditPassedCon}         creditPassedCon
${creditDownCon}           creditDownCon
${considertime}            considertime

***Keyword***
เปิดหน้าจอ
    [Arguments]             ${link_web}    ${web_browser}
    [Documentation]         ใช้เปิดหน้าจอ
    Open Browser            ${link_web}    ${web_browser}

กรอกข้อมูล
    [Arguments]             ${var}    ${txt}
    [Documentation]         กรอกข้อมูล
    Input Text              ${var}    ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         กดปุ่ม
    Click Button            ${btn}

กดลิงค์รูป
    [Arguments]             ${link_page}
    [Documentation]         ใช้กดลิงค์
    Click Link              ${link_page}

***Test Case ***

TC-PSF-03-07-01 ตรวจสอบการกรอกข้อมูลรหัสนิสิต

    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuID}                      ${txt_stuID} 

TC-PSF-03-07-02 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuName}                    ${txt_name}

TC-PSF-03-07-03 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuGpaCon}                  ${txt_gpa}

TC-PSF-03-07-04 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${creditPassedCon}            ${txt_crditPass}

TC-PSF-03-07-05 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${creditDownCon}              ${txt_crditDownCon}

TC-PSF-03-07-06 ตรวจสอบการกรอกข้อมูลรหัส นิสิต รับไม่เกิน 8 ตัวอักษร และ ไม่น้อยกว่า 8 ตัวอักษร
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuID}                      ${txt_stuID2} 

TC-PSF-03-07-07 ตรวจสอบการกรอกข้อมูลชื่อ- นามสกุล รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuName}                    ${txt_name2}

TC-PSF-03-04-08 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย รับทศนิยมไม่เกิน 2 หลัก และทศนิยมไม่น้อยกว่า 2 หลัก
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuGpaCon}                  ${txt_gpa2}

TC-PSF-03-07-09 ตรวจสอบการกรอกข้อมูลหน่วย กิตที่ผ่าน รับเลขไม่เกิน 3 หลัก
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${creditPassedCon}            ${txt_crditPass2}

TC-PSF-03-07-10 ตรวจสอบการกรอกข้อมูลหน่วย กิตลงรับเลขไม่เกิน 3 หลัก
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${creditDownCon}              ${txt_crditDownCon2}

TC-PSF-03-07-11 ตรวจสอบการกรอกหากไม่กรอก ข้อมูลรหัสนิสิต
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuID}                      ${txt_stuID3} 

TC-PSF-03-07-12 ตรวจสอบการกรอกหากไม่กรอก ข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuName}                    ${txt_name3}

TC-PSF-03-07-13 ตรวจสอบการกรอกหากไม่กรอก ข้อมูลเกรดเฉลี่ย
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuGpaCon}                  ${txt_gpa3}

TC-PSF-03-07-14 ตรวจสอบการกรอกหากไม่กรอก ข้อมูลหน่วยกิตที่ผ่าน
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${creditPassedCon}            ${txt_crditPass3}

TC-PSF-03-07-15 ตรวจสอบการกรอกหากไม่กรอก ข้อมูลหน่วยกิตที่ลง
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${creditDownCon}              ${txt_crditDownCon3}

TC-PSF-03-07-16 ตรวจสอบการกรอกข้อมูลรหัส นิสิตเป็นตัวอักษรมั่ว
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuID}                      ${txt_stuID4} 

TC-PSF-03-07-17 ตรวจสอบการกรอกข้อมูลชื่อ- นามสกุลเป็นตัวอักษรมั่ว
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuName}                    ${txt_name4}

TC-PSF-03-07-18 ตรวจสอบการกรอกข้อมูลเกรด เฉลี่ยเป็นตัวอักษรมั่ว
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${stuGpaCon}                  ${txt_gpa4}

TC-PSF-03-07-19 ตรวจสอบการกรอกข้อมูลหน่วย กิตที่ผ่านเป็นตัวอักษรมั่ว
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${creditPassedCon}            ${txt_crditPass4}

TC-PSF-03-07-20 ตรวจสอบการกรอกข้อมูลหน่วย กิตที่ลงเป็นตัวอักษรมั่ว
    เปิดหน้าจอ                ${URL_manageStuCon}           ${web_browser}
    กดลิงค์รูป                 ${btnEdit}                 
    กรอกข้อมูล                ${creditDownCon}              ${txt_crditDownCon4}





















